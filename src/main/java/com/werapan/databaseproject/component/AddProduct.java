/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.werapan.databaseproject.component;

import com.werapan.databaseproject.model.Product;

/**
 *
 * @author EliteCorps
 */
public interface AddProduct {
    public void addProduct(Product product, int qty);
}

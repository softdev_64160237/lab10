/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import com.werapan.databaseproject.dao.CustomerDao;
import com.werapan.databaseproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author EliteCorps
 */
public class Reciept {

    private int id;
    private Date createDate;
    private float total;
    private int totalQTY;
    private float cash;
    private float change;
    private int customerID;
    private int userID;
    private Customer customer;
    private User user;
    private ArrayList<RecieptDetail> recDetails = new ArrayList();

    public Reciept(int id, Date createDate, float total, int totalQTY, float cash, float change, int customerID, int userID) {
        this.id = id;
        this.createDate = createDate;
        this.total = total;
        this.totalQTY = totalQTY;
        this.cash = cash;
        this.change = change;
        this.customerID = customerID;
        this.userID = userID;
    }

    public Reciept(Date createDate, float total, int totalQTY, float cash, float change, int customerID, int userID) {
        this.id = -1;
        this.createDate = createDate;
        this.total = total;
        this.totalQTY = totalQTY;
        this.cash = cash;
        this.change = change;
        this.customerID = customerID;
        this.userID = userID;
    }

    public Reciept(float total, int totalQTY, float cash, float change, int customerID, int userID) {
        this.id = -1;
        this.createDate = null;
        this.total = total;
        this.totalQTY = totalQTY;
        this.cash = cash;
        this.change = change;
        this.customerID = customerID;
        this.userID = userID;
    }

    public Reciept(float cash, float change, int customerID, int userID) {
        this.id = -1;
        this.createDate = null;
        this.total = 0;
        this.totalQTY = 0;
        this.cash = cash;
        this.change = change;
        this.customerID = customerID;
        this.userID = userID;
    }

    public Reciept() {
        this.id = -1;
        this.createDate = null;
        this.total = 0;
        this.totalQTY = 0;
        this.cash = 0;
        this.change = 0;
        this.customerID = 0;
        this.userID = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getTotalQTY() {
        return totalQTY;
    }

    public void setTotalQTY(int totalQTY) {
        this.totalQTY = totalQTY;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public float getChange() {
        return change;
    }

    public void setChange(float change) {
        this.change = change;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        this.customerID = customer.getId();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userID = user.getId();
    }

    public ArrayList<RecieptDetail> getRecDetails() {
        return recDetails;
    }

    public void setRecDetails(ArrayList<RecieptDetail> recDetails) {
        this.recDetails = recDetails;
    }

    @Override
    public String toString() {
        return "Reciept{" + "id=" + id + ", createDate=" + createDate + ", total=" + total + ", totalQTY=" + totalQTY + ", cash=" + cash + ", change=" + change + ", customerID=" + customerID + ", userID=" + userID + ", customer=" + customer + ", user=" + user + ", recDetails=" + recDetails + '}';
    }

    public void addRecieptDetail(RecieptDetail recDetail) {
        recDetails.add(recDetail);
        calculateTotal();
    }

    public void addRecieptDetail(Product product, int qty) {
        for (RecieptDetail rd : recDetails) {
            if (rd.getProductID() == product.getId()) {
                int currentQty = rd.getQty();
                int newQty = currentQty + qty;
                rd.setQty(newQty);
                rd.setTotal(rd.getPrice() * newQty);
                calculateTotal();
                return;
            }
        }
        RecieptDetail rd = new RecieptDetail(product.getId(), product.getName(), product.getPrice(), qty, product.getPrice() * qty, -1);
        recDetails.add(rd);
        calculateTotal();
    }

    public void delRecieptDetail(RecieptDetail recDetail) {
        recDetails.remove(recDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for (RecieptDetail recDetail : recDetails) {
            total += recDetail.getTotal();
            totalQty += recDetail.getQty();
        }
        this.total = total;
        this.totalQTY = totalQty;
    }

    public static Reciept fromRS(ResultSet rs) {
        Reciept reciept = new Reciept();
        try {
            reciept.setId(rs.getInt("rec_id"));
            reciept.setCreateDate(rs.getTimestamp("rec_date"));
            reciept.setTotal(rs.getFloat("rec_total"));
            reciept.setTotalQTY(rs.getInt("rec_total_qty"));
            reciept.setCash(rs.getFloat("rec_cash"));
            reciept.setChange(rs.getFloat("rec_change"));
            reciept.setCustomerID(rs.getInt("customer_id"));
            reciept.setUserID(rs.getInt("user_id"));
            //Population
            CustomerDao cusDao = new CustomerDao();
            UserDao userDao = new UserDao();
            Customer customer = cusDao.get(reciept.getCustomerID());
            User user = userDao.get(reciept.getUserID());
            reciept.setCustomer(customer);
            reciept.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }
}

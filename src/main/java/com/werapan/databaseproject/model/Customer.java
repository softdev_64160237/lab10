/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author EliteCorps
 */
public class Customer {
    private int id;
    private String fName;
    private String lName;
    private Date startDate;
    private String tel;
    private int point;

    public Customer(int id, String fName, String lName, Date startDate, String tel, int point) {
        this.id = id;
        this.fName = fName;
        this.lName = lName;
        this.startDate = startDate;
        this.tel = tel;
        this.point = point;
    }

    public Customer(String fName, String lName, Date startDate, String tel, int point) {
        this.id = -1;
        this.fName = fName;
        this.lName = lName;
        this.startDate = startDate;
        this.tel = tel;
        this.point = point;
    }

    public Customer() {
        this.id = -1;
        this.fName = "";
        this.lName = "";
        this.startDate = null;
        this.tel = "";
        this.point = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", fName=" + fName + ", lName=" + lName + ", startDate=" + startDate + ", tel=" + tel + ", point=" + point + '}';
    }
    
    public static Customer fromRS(ResultSet rs) {
        Customer customer = new Customer();
        try {
            customer.setId(rs.getInt("customer_id"));
            customer.setfName(rs.getString("customer_fname"));
            customer.setlName(rs.getString("customer_lname"));
            customer.setStartDate(rs.getTimestamp("customer_start_date"));
            customer.setTel(rs.getString("customer_tel"));
            customer.setPoint(rs.getInt("customer_point"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return customer;
    }
}

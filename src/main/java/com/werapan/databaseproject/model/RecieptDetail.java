/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author EliteCorps
 */
public class RecieptDetail {
    private int id;
    private int productID;
    private String name;
    private float price;
    private int qty;
    private float total;
    private int recieptID;

    public RecieptDetail(int id, int productID, String name, float price, int qty, float total, int recieptID) {
        this.id = id;
        this.productID = productID;
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.total = total;
        this.recieptID = recieptID;
    }

    public RecieptDetail(int productID, String name, float price, int qty, float total, int recieptID) {
        this.id = -1;
        this.productID = productID;
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.total = total;
        this.recieptID = recieptID;
    }

    public RecieptDetail() {
        this.id = -1;
        this.productID = 0;
        this.name = "";
        this.price = 0;
        this.qty = 0;
        this.total = 0;
        this.recieptID = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
        total = qty * price;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getRecieptID() {
        return recieptID;
    }

    public void setRecieptID(int recieptID) {
        this.recieptID = recieptID;
    }

    @Override
    public String toString() {
        return "RecieptDetail{" + "id=" + id + ", productID=" + productID + ", name=" + name + ", price=" + price + ", qty=" + qty + ", total=" + total + ", recieptID=" + recieptID + '}';
    }
    
    public static RecieptDetail fromRS(ResultSet rs) {
        RecieptDetail recDetail = new RecieptDetail();
        try {
            recDetail.setId(rs.getInt("rcd_id"));
            recDetail.setProductID(rs.getInt("product_id"));
            recDetail.setName(rs.getString("rcd_name"));
            recDetail.setPrice(rs.getFloat("rcd_price"));
            recDetail.setQty(rs.getInt("rcd_qty"));
            recDetail.setTotal(rs.getFloat("rcd_total"));
            recDetail.setRecieptID(rs.getInt("rec_id"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recDetail;
    }
}

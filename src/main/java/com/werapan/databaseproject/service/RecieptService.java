/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.RecieptDao;
import com.werapan.databaseproject.dao.RecieptDetailDao;
import com.werapan.databaseproject.model.Reciept;
import com.werapan.databaseproject.model.RecieptDetail;
import java.util.List;

/**
 *
 * @author EliteCorps
 */
public class RecieptService {
    RecieptDao recDao = new RecieptDao();
    RecieptDetailDao rdd = new RecieptDetailDao();
    
    public Reciept getRecieptByID(int id) {
        return recDao.get(id);
    }
    
    public List<Reciept> getReciepts() {
        return recDao.getAll(" rec_id asc");
    }
    
    public Reciept addReciept(Reciept editedReciept) {
        Reciept reciept = recDao.save(editedReciept);
        for (RecieptDetail rd : editedReciept.getRecDetails()) {
            rd.setRecieptID(reciept.getId());
            rdd.save(rd);
        }
        return reciept;
    }
    
    public Reciept updateReciept(Reciept editedReciept) {
        return recDao.update(editedReciept);
    }
    
    public int deleteReciept(Reciept editedReciept) {
        return recDao.delete(editedReciept);
    }
}

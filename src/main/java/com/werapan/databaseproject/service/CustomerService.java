/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.CustomerDao;
import com.werapan.databaseproject.model.Customer;
import java.util.List;

/**
 *
 * @author EliteCorps
 */
public class CustomerService {
    CustomerDao cusDao = new CustomerDao();
    
    public Customer getCustomerBytel(String tel) {
        Customer customer = cusDao.getByTell(tel);
        return customer;
    }
    
    public List<Customer> getCustomers() {
        return cusDao.getAll(" customer_id asc");
    }
    
    public Customer addCustomer(Customer editedCustomer) {
        return cusDao.save(editedCustomer);
    }
    
    public Customer updateCustomer(Customer editedCustomer) {
        return cusDao.update(editedCustomer);
    }
    
    public int deleteCustomer(Customer editedCustomer) {
        return cusDao.delete(editedCustomer);
    }
}

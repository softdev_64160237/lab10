/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.ProductDao;
import com.werapan.databaseproject.model.Product;
import java.util.List;

/**
 *
 * @author EliteCorps
 */
public class ProductService {
    ProductDao productDao = new ProductDao();
    
    public List<Product> getProducts() {
        return productDao.getAll(" product_id asc");
    }
    
    public Product addProduct(Product editedProduct) {
        return productDao.save(editedProduct);
    }
    
    public Product updateProduct(Product editedProduct) {
        return productDao.update(editedProduct);
    }
    
    public int deleteProduct(Product editedProduct) {
        return productDao.delete(editedProduct);
    }
}
